export class DestinoViaje {
    private selected: boolean;
    public servicios: string[];
    /* constructor(n:string, u:string){
        this.nombre=n;
        this.imagenUrl=u;
    }*/
    constructor(public nombre: string, public u: string){
        this.servicios= ['pileta','desayuno'];
    }
    isSelected(): boolean {
        return this.selected;
    }

    setSelected(s: boolean) {
        this.selected = s;
    }
}