import { BehaviorSubject, Subject } from 'rxjs';
import { DestinoViaje } from './destino-viaje.model';

export class DestinosApiClient {
	destinos:DestinoViaje[];

	//objeto observable: le vamos a publicar cual es el actual destino que es favorito. Vamos a hacerlo observable para que otros puedan suscribirse a las actualizaciones de cuando hay un nuevo objeto seteado como favorito.
	current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);
	constructor() {
       this.destinos = [];
	}
	add(d:DestinoViaje){
	  this.destinos.push(d);
	}
	getAll(){
	  return this.destinos;
    }

	grtById(id: String): DestinoViaje{
		return this.destinos.filter(function(d){return d.isSelected.toString()===id;})[0];
	}

	elegir(d: DestinoViaje){
		this.destinos.forEach(x=>x.setSelected(false));
		d.setSelected(true);
		this.current.next(d);
	}
	
	//Para que se suscriban a las actualizaciones.
	subscribeOnChange(fn){
		this.current.subscribe(fn);
	}
}