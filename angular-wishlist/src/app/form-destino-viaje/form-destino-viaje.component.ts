import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { OutletContext } from '@angular/router';
import { fromEvent } from 'rxjs';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { map, filter, debounce, distinctUntilChanged, switchMap, debounceTime } from 'rxjs/operators';
import { DestinoViaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
@Output() onItemAdded: EventEmitter<DestinoViaje>;
fg: FormGroup;
minLongitud=3;
searchResults: string[];

  constructor(fb: FormBuilder) {
    this.onItemAdded= new EventEmitter();
    this.fg= fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametrizable(this.minLongitud)
      ])],
      url: ['', Validators.required]
    });
     //observador de tipeo
     this.fg.valueChanges.subscribe((form: any) =>{
      console.log('cambio el formulario: ', form);
    })
   }

   //map: valor de la tecla que apreto,filter: para wue busque cuando ingreso mas de 2 letras, debounceTime: para que se quede en stop 2 milisegundos, esto para evitar hacer busqueda con cada letra nueva que ingreso, destinctUntilChanged: si me llega la misma cadena(escribo y borro letra y dentro de los 2 ms me termina llegando la misma cadena), no me deja avanzar con el flujo. Por lo que se agrega esta funcion para ignorar las cadenas que son iguales.
   //ajax: Alli es donde se psaaria el texto que llega a un web service. Ahora se simula con un archivo.
  ngOnInit(): void {
    let elemNombre=<HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre,'input')
      .pipe(
        map((e:KeyboardEvent)=>(e.target as HTMLInputElement).value),
        filter(text=>text.length>2),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(()=>ajax('/assets/datos.json'))
      ).subscribe(AjaxResponse=>{
        this.searchResults= AjaxResponse.response;
      });
  }
guardar(nombre: string, url: string): boolean{
  let d= new DestinoViaje(nombre,url);
  this.onItemAdded.emit(d);
  return false;
}

//Validador personalizado

// { [s:string]:boolean } : especifico tipo de dato (objeto en este caso), compuesto por una key(s:string) y un valor ("parecido" a json)
nombreValidator(control: FormControl): { [s:string]:boolean }{
  let l= control.value.toString().trim().length;
  if(l>0 && l<5){
    return {invalidNombre: true}
  }
  return null;
}

//Validador parametrizable
//ValidatorFn: Una función que recibe un control y devuelve sincrónicamente un mapa de errores de validación si está presente,de lo contrario es nula.

nombreValidatorParametrizable(minLong: number): ValidatorFn{

  //retorno una funcion(escrita con expresion lambda)
  return(control:FormControl): {[s:string]: boolean} | null =>{
    let l= control.value.toString().trim().length;
    if(l>0 && l<minLong){
      return {minLongNombre: true}
    }
    return null;
  }
}


}
